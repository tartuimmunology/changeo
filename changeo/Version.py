"""
Version and authorship information
"""

__author__    = 'Namita Gupta, Jason Anthony Vander Heiden'
__copyright__ = 'Copyright 2021 Kleinstein Lab, Yale University. All rights reserved.'
__license__   = 'GNU Affero General Public License 3 (AGPL-3)'
__version__   = '1.2.0'
__date__      = '2021.10.29'
